<?php

/**
 * Class MySingleton
 */
final class MySingleton
{
    /**
     * @var MySingleton|null
     */
    private static $instance;

    /**
     * MySingleton constructor. Please, note "private" visibility.
     */
    private function __construct()
    {
    }

    /**
     * @return MySingleton
     */
    public static function getInstance(): MySingleton
    {
        if (self::$instance === null) {
            self::$instance = new MySingleton();
        }

        return self::$instance;
    }

    public function __clone()
    {
        throw new Exception('Class implementing Singleton pattern cannot support cloning of instances.');
    }

    public function __wakeup()
    {
        throw new Exception('Class implementing Singleton pattern cannot support unserialization.');
    }
}
