<?php

/**
 * Class Comment
 */
class Comment extends Node
{
    /**
     * @var string
     */
    protected $text;

    /**
     * Node constructor.
     * @param int $id
     * @param int $parentId
     * @param string $text
     */
    public function __construct(int $id, int $parentId, string $text)
    {
        parent::__construct($id, $parentId);
        $this->text = $text;
    }

    public function getRenderedData(): string
    {
        return $this->text;
    }
}
