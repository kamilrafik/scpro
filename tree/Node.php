<?php

/**
 * Class Node
 */
abstract class Node
{
    abstract function getRenderedData(): string;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $parentId;

    /**
     * Node constructor.
     * @param int $id
     * @param int $parentId
     */
    public function __construct(int $id, int $parentId)
    {
        $this->id = $id;
        $this->parentId = $parentId == $id ? null : $parentId;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getParentId(): ?int
    {
        return $this->parentId;
    }
}
