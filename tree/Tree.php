<?php

class Tree
{
    /**
     * @var Node[]
     */
    protected $nodes = [];

    /**
     * @param Node $node
     */
    public function addNode(Node $node): void
    {
        $this->nodes[] = $node;
    }

    /**
     * @param null|int $parentId
     * @return string
     */
    public function render($parentId = null): string
    {
        if ($parentId === null) {
            usort($this->nodes, function (Node $a, Node $b) {
                return $a->getId() < $b->getId() ? -1 : 1;
            });
        }

        $nodes = array_filter(
            $this->nodes,
            function (Node $node) use ($parentId) {
                return $node->getParentId() == $parentId;
            },
            ARRAY_FILTER_USE_BOTH
        );

        $html = '';
        foreach ($nodes as $node) {
            $html .=
                Html::div(Html::encode($node->getRenderedData()))
                . $this->render($node->getId())
            ;
        }
        if (count($nodes)) {
            $html = Html::div($html);
        }

        return $html;
    }
}
