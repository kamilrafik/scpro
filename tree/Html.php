<?php

class Html
{
    /**
     * @param string $html
     * @return string
     */
    public static function div($html)
    {
        return '<div>' . $html . '</div>';
    }

    /**
     * @param string $text
     * @return string
     */
    public static function encode($text)
    {
        return htmlspecialchars(
                $text,
                ENT_QUOTES | ENT_SUBSTITUTE,
                'UTF-8',
                true
            );
    }

    /**
     * Dirty semi-working "pretty print" solution just to render results in test task in a readable way.
     *
     * @param string $html
     * @return string
     */
    public static function prettyPrint($html)
    {
        $document = new DOMDocument();
        $document->preserveWhiteSpace = false;
        $document->loadHTML($html);
        $document->formatOutput = true;
        return $document->saveXML();
    }
}