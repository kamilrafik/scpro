<?php

use yii\db\Migration;

/**
 * Class m220424_173216_init_data
 */
class m220424_173216_init_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert(
            '{{%organization}}',
            ['name'],
            [
                ['organization 1'],
                ['organization 2'],
                ['organization 3'],
                ['organization 4'],
                ['organization 5'],
            ]
        );

        $this->batchInsert(
            '{{%schedule}}',
            ['organization_id', 'day_of_week', 'open', 'close'],
            [
                [1, 1, 0, 23 * 60],
                [2, 1, 9 * 60, 21 * 60],
                [3, 1, 12 * 60, 21 * 60],
                [4, 1, 15 * 60, 24 * 60],
                // overnight
                [5, 1, 18 * 60, 5 * 60],

                [1, 2, 0, 23 * 60],
                [2, 2, 9 * 60, 21 * 60],
                [3, 2, 12 * 60, 21 * 60],
                [4, 2, 15 * 60, 24 * 60],
                // overnight
                [5, 2, 18 * 60, 5 * 60],
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('{{%schedule}}');

        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $this->truncateTable('{{%organization}}');
        $this->execute('SET FOREIGN_KEY_CHECKS=1');
    }
}
