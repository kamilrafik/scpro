<?php

use yii\db\Migration;

/**
 * Class m220424_162121_init
 */
class m220424_162121_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            '{{%organization}}',
            [
                'id' => $this->primaryKey(),
                'name' => $this->string()->unique(),
            ]
        );

        $this->createTable(
            '{{%schedule}}',
            [
                'id' => $this->primaryKey(),
                'organization_id' => $this->integer()->notNull(),
                'day_of_week' => $this->tinyInteger()->notNull(),
                'open' => $this->integer()->notNull(),
                'close' => $this->integer()->notNull(),
            ]
        );

        $this->addForeignKey(
            'fk-schedule-organization',
            '{{%schedule}}',
            'organization_id',
            '{{%organization}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-schedule-organization', '{{%schedule}}');
        $this->dropTable('{{%schedule}}');
        $this->dropTable('{{%organization}}');
    }
}
