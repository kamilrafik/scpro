<?php

namespace app\models\query;

use app\models\Organization;
use app\models\Schedule;
use yii\db\ActiveQuery;
use yii\db\Expression;

class OrganizationQuery extends ActiveQuery
{
    public function open($time = null, $addRequestedTime = true)
    {
        Schedule::convertTime($time, $dayOfWeek, $minute);
        if ($addRequestedTime) {
            $this->select([
                Organization::tableName() . '.*',
                new Expression(intval($time) . ' AS requested_time')
            ]);
        }
        return $this
            ->innerJoinWith([
                'schedules' => function (ActiveQuery $query) use ($dayOfWeek, $minute) {
                    $query->andWhere([
                        'AND',
                        [Schedule::tableName() . '.day_of_week' => $dayOfWeek],
                        ['<=', Schedule::tableName() . '.open', $minute],
                        [
                            'OR',
                            ['>', Schedule::tableName() . '.close', $minute],
                            // for overnight organizations
                            new Expression(Schedule::tableName() . '.close < ' . Schedule::tableName() . '.open'),
                        ],
                    ]);
                },
            ]);
    }

    public function closed($time = null)
    {
        return $this
            ->select([
                Organization::tableName() . '.*',
                new Expression(intval($time) . ' AS requested_time')
            ])
            ->andWhere([
                'NOT IN',
                Organization::tableName() . '.id',
                Organization::find()->select(Organization::tableName() . '.id')->open($time, false)
            ]);
    }
}