<?php

namespace app\models\rest;

use app\models\Organization;
use app\models\Schedule;
use Yii;

class OpenOrganization extends Organization
{
    public $requested_time;

    public function fields()
    {
        return [
            'name',
            'openTimespan',
        ];
    }

    /**
     * @see README.md
     * @return string
     */
    public function getOpenTimespan()
    {
        Schedule::convertTime($this->requested_time, $dayOfWeek, $minute);

        /** @var Schedule $schedule */
        $schedule = $this->getSchedules()->andWhere([
            'day_of_week' => $dayOfWeek,
        ])->one();

        $diff = 0;
        if ($schedule->close > $schedule->open) {
            $diff = $schedule->close - (date('H', $this->requested_time) * 60 + date('i', $this->requested_time));
        } else {
            $diff = (24 * 60 - (date('H', $this->requested_time) * 60 + date('i', $this->requested_time))) + $schedule->close;
        }

        return Yii::t('app', '{hrs} hrs {min} min', [
            'hrs' => intval($diff / 60),
            'min' => $diff % 60,
        ]);
    }
}