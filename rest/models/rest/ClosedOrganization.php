<?php

namespace app\models\rest;

use app\models\Organization;
use app\models\Schedule;
use Yii;

class ClosedOrganization extends Organization
{
    public $requested_time;

    public function fields()
    {
        return [
            'name',
            'closedTimespan',
        ];
    }

    /**
     * @see README.md
     * @return string
     */
    public function getClosedTimespan()
    {
        Schedule::convertTime($this->requested_time, $dayOfWeek, $minute);

        $diff = 0;
        for ($i = 1; $i <=7; $i++) {
            /** @var Schedule $schedule */
            $schedule = $this->getSchedules()->andWhere([
                'day_of_week' => $dayOfWeek,
            ])->one();
            if ($schedule === null) {
                $diff += ($i == 1) ? (24 * 60 - $minute) : (24 * 60);
            } else {
                if ($i == 1) {
                    if ($minute < $schedule->open) {
                        $diff = $schedule->open - $minute;
                        break;
                    } else {
                        $diff = 24 * 60 - $minute;
                    }
                } else {
                    $diff += $schedule->open;
                    break;
                }
            }
            if (++$dayOfWeek == 8) {
                $dayOfWeek = 1;
            }
        }

        return Yii::t('app', '{hrs} hrs {min} min', [
            'hrs' => intval($diff / 60),
            'min' => $diff % 60,
        ]);
    }
}