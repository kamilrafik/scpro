<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "schedule".
 *
 * @property int $id
 * @property int $organization_id
 * @property int $day_of_week
 * @property int $open
 * @property int $close
 *
 * @property Organization $organization
 */
class Schedule extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%schedule}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['organization_id', 'day_of_week', 'open', 'close'], 'required'],
            [['organization_id', 'day_of_week', 'open', 'close'], 'integer'],
            [['day_of_week'], 'number', 'min' => 1, 'max' => 7,],
            [['open', 'close'], 'number', 'min' => 0, 'max' => 24 * 60,],
            // if organizations closes at the next day morning it may have "close" value less than "open" value
            [['close'], 'compare', 'compareAttribute' => 'open', 'operator' => '!='],
            [['organization_id'], 'exist', 'skipOnError' => true, 'targetClass' => Organization::class, 'targetAttribute' => ['organization_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'organization_id' => Yii::t('app', 'Organization ID'),
            'day_of_week' => Yii::t('app', 'Day Of Week'),
            'open' => Yii::t('app', 'Open'),
            'close' => Yii::t('app', 'Close'),
        ];
    }

    /**
     * Gets query for [[Organization]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrganization()
    {
        return $this->hasOne(Organization::className(), ['id' => 'organization_id']);
    }

    public static function convertTime($time, &$dayOfWeek, &$minute)
    {
        $dayOfWeek = date('w', $time) ?: 7;
        $minute = date('H', $time) * 60 + date('i', $time);
    }
}
