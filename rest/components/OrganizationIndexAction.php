<?php

namespace app\components;

use yii\rest\IndexAction;
use Yii;

class OrganizationIndexAction extends IndexAction
{
    public $isOpen = false;

    public function run()
    {
        $dataProvider = parent::run();

        $time = Yii::$app->request->get('time', time());

        if ($this->isOpen) {
            $dataProvider->query->open($time);
        } else {
            $dataProvider->query->closed($time);
        }

        return $dataProvider;
    }
}
