<?php

namespace app\controllers;

use app\components\OrganizationIndexAction;
use app\models\rest\ClosedOrganization;
use app\models\rest\OpenOrganization;
use yii\rest\ActiveController;

class OrganizationController extends ActiveController
{
    public $modelClass = 'app\models\Organization';

    public function actions()
    {
        return [
            'open' => [
                'class' => OrganizationIndexAction::class,
                'modelClass' => OpenOrganization::class,
                'isOpen' => true,
            ],
            'closed' => [
                'class' => OrganizationIndexAction::class,
                'modelClass' => ClosedOrganization::class,
                'isOpen' => false,
            ],
        ];
    }

    protected function verbs()
    {
        return [
            'open' => ['GET', 'HEAD'],
            'closed' => ['GET', 'HEAD'],
        ];
    }
}