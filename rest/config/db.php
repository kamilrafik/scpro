<?php

use yii\helpers\ArrayHelper;

return ArrayHelper::merge(
    [
        'class' => 'yii\db\Connection',
        'charset' => 'utf8',
        // 'dsn' => 'mysql:host=localhost;dbname=scpro_test',
        // 'username' => 'scpro_test',
        // 'password' => 'scpro_test',
    ],
    require_once __DIR__ . '/db-local.php'
);
