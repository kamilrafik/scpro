<?php

require_once __DIR__ . '/patterns/MySingleton.php';

$singleton = MySingleton::getInstance();
var_dump($singleton);
echo "\n\n";

////////////////////////////////////////////////////////

require_once __DIR__ . '/tree/Node.php';
require_once __DIR__ . '/tree/Comment.php';
require_once __DIR__ . '/tree/Tree.php';
require_once __DIR__ . '/tree/Html.php';

$commentRecords = [
    [4, 1, 'Comment 4'],
    [5, 2, 'Comment 5'],
    [6, 3, 'Comment 6'],
    [7, 7, 'Comment 7'],
    [1, 1, 'Comment 1'],
    [2, 1, 'Comment 2'],
    [3, 2, 'Comment 3'],
];

$tree = new Tree();
foreach ($commentRecords as $commentRecord) {
    $comment = new Comment($commentRecord[0], $commentRecord[1], $commentRecord[2]);
    $tree->addNode($comment);
}

echo Html::prettyPrint($tree->render());

////////////////////////////////////////////////////////

echo "\n\n=====================\n\n";

$times = [
    '2022-04-24 12:00',
    '2022-04-25 12:00',
    '2022-04-26 12:00',
    '2022-04-27 12:00',
];

foreach ($times as $time) {
    echo $time . "\n\n";
    echo exec('curl -is -H "Accept:application/json" "http://scpro-test.localhost/organizations/open/' . strtotime($time) . '"');
    echo "\n\n";
    echo exec('curl -is -H "Accept:application/json" "http://scpro-test.localhost/organizations/closed/' . strtotime($time) . '"');
    echo "\n\n=====================\n\n";
}