Test task has been developed with use of PHP 7.2 due to two reasons: 

a) I use this version currently to support one legacy project and newer version of PHP is not installed locally;

b) my old perpetual license for PhpStorm which supports PHP 7 only cannot be upgraded because JetBrains does not sell anything to Russia now, and searching/installing new IDE for test task only would took too much time.

///////////////////////////

Implementation details of REST API:

a) for simplicity, processing of organizations that work 24hrs per day has not been implemented;

b) for simplicity, it was supposed that even if organization works overnight there will be break after it will stop working; so, overlapping of work hours in consecutive days is not supported;

c) for simplicity, it was supposed that every organization works at least one day per week;

d) for simplicity, only JSON output processing has been implemented;

e) for simplicity, no versioning, authentication, limitation of filtering etc has been implemented;

f) in fact, in production environment timespans calculation logic could be even more complex than it can be imagined and it really requires unit tests to be developed; but here, for simplicity, only basic testing in run.php has been implemented to be sure that basically al works. 

///////////////////////////

Installation instruction:

a) create database and user and configure them in rest/config/db-local.php;

b) run "compposer install" in "rest" directory;

c) configure "scpro-test.localhost" virtual host to point at "rest/web" directory;

d) run "run.php" in root directory.




